
/* vuex State Management Pattern
* https://vuex.vuejs.org/
* */
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);

/*
* store modules
* */
import carsData from './modules/carsData'
import insurance from './modules/insurance'

export default new Vuex.Store({
    modules: {
        carsData,
        insurance
    }
})
