@extends('layouts.app')
@section('title', 'Register')
@section('content')
    <register route_register="{{route('register')}}"
              old_name="{{old('name')}}"
              name_error="{{$errors->has('name')}}"
              email_error="{{$errors->has('email')}}"
              password_error="{{$errors->has('password')}}"
              message="{{json_encode($errors->all())}}" old_email="{{old('email')}}"></register>
@endsection
