<?php
/**
 * Created by PhpStorm.
 * User: kar
 * Date: 5/20/20
 * Time: 6:56 PM
 */

namespace App\Http\Repositories;


use App\Http\Contracts\MarkInterface;
use App\Models\Mark;

/**
 * Class MarkRepository
 * @package App\Http\Repositories
 */
class MarkRepository implements MarkInterface
{
    protected $model;

    /**
     * MarkRepository constructor.
     * @param Mark $model
     */
    public function __construct(Mark $model)
    {
        $this->model = $model;
    }

    /**
     * @return CarMake[]|\Illuminate\Database\Eloquent\Collection
     */
    public function get()
    {
        return $this->model->all();
    }
}
