<?php
/**
 * Created by PhpStorm.
 * User: kar
 * Date: 5/21/20
 * Time: 12:34 AM
 */

namespace App\Http\Repositories;


use App\Http\Contracts\InsuranceInterface;
use App\Models\Insurance;

/**
 * Class InsuranceRepository
 * @package App\Http\Repositories
 */
class InsuranceRepository implements InsuranceInterface
{
    protected $model;

    /**
     * InsuranceRepository constructor.
     * @param Insurance $model
     */
    public function __construct(Insurance $model)
    {
        $this->model = $model;
    }
    /**
     * @param $data
     * @return mixed|void
     */
    public function create($data)
    {
        $this->model->updateOrCreate(['user_id' => $data['user_id']], $data);
    }

    /**
     * @param $userId
     * @return mixed|void
     */
    public function getOne($userId)
    {
        return $this->model->where('user_id', $userId)->first();
    }
}
