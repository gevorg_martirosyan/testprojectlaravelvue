<?php
/**
 * Created by PhpStorm.
 * User: kar
 * Date: 5/20/20
 * Time: 10:08 PM
 */

namespace App\Http\Repositories;

use App\Http\Contracts\DrivetrainInterface;
use App\Models\Drivetrain;

/**
 * Class DrivetrainRepository
 * @package App\Http\Repositories
 */
class DrivetrainRepository implements DrivetrainInterface
{
    protected $model;

    /**
     * DrivetrainRepository constructor.
     * @param Drivetrain $model
     */
    public function __construct(Drivetrain $model)
    {
        $this->model = $model;
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function get($type)
    {
        return $this->model->where('type_id', $type)->get();
    }
}
