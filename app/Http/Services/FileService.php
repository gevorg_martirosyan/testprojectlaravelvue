<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

/**
 * Class FileService
 * @package App\Http\Services
 */
class FileService
{
    /**
     * @param $path
     * @param $file
     */
    public function upload($path, $file)
    {
        Storage::disk('public')->putFile($path, $file);
    }
}
