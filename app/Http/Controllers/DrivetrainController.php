<?php

namespace App\Http\Controllers;
use App\Http\Contracts\DrivetrainInterface;

/**
 * Class DrivetrainController
 * @package App\Http\Controllers
 */
class DrivetrainController extends Controller
{
    protected $drivetrainRepo;

    /**
     * DrivetrainController constructor.
     * @param DrivetrainInterface $drivetrainRepo
     */
    public function __construct(DrivetrainInterface $drivetrainRepo)
    {
        $this->drivetrainRepo = $drivetrainRepo;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        $type = request()->type;
        if ($type) {
            $data = $this->drivetrainRepo->get($type);
            return response()->json([
                'success' => 1,
                'type'    => 'success',
                'data'    => $data
            ]);
        } else {
            return response()->json([
                'success' => 0,
                'type'    => 'error',
                'message' => 'Please select model'
            ]);
        }
    }
}
