<?php

namespace App\Http\Controllers;

use App\Http\Contracts\MarkInterface;

/**
 * Class MarkController
 * @package App\Http\Controllers
 */
class MarkController extends Controller
{
    protected $markRepo;

    /**
     * MarkController constructor.
     * @param MarkInterface $markRepo
     */
    public function __construct(MarkInterface $markRepo)
    {
        $this->markRepo = $markRepo;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        $data = $this->markRepo->get();
        return response()->json([
            'success' => 1,
            'type'    => 'success',
            'data'    => $data
        ]);
    }
}
