<?php

namespace App\Http\Controllers;

use App\Http\Contracts\TypeInterface;

/**
 * Class TypeController
 * @package App\Http\Controllers
 */
class TypeController extends Controller
{
    protected $typeRepo;

    /**
     * TypeController constructor.
     * @param TypeInterface $typeRepo
     */
    public function __construct(TypeInterface $typeRepo)
    {
        $this->typeRepo = $typeRepo;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        $mark = request()->mark;
        if ($mark) {
            $data = $this->typeRepo->get($mark);
            return response()->json([
                'success' => 1,
                'type'    => 'success',
                'data'    => $data
            ]);
        } else {
            return  response()->json([
                'success' => 0,
                'type'    => 'error',
                'message' => 'Please select mark'
            ]);
        }
    }
}
