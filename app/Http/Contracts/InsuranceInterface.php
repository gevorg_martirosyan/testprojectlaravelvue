<?php
/**
 * Created by PhpStorm.
 * User: kar
 * Date: 5/21/20
 * Time: 12:35 AM
 */

namespace App\Http\Contracts;

/**
 * Interface InsuranceInterface
 * @package App\Http\Contracts
 */
interface InsuranceInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function create($data);

    /**
     * @param $userId
     * @return mixed
     */
    public function getOne($userId);
}
