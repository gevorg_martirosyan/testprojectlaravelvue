<?php
/**
 * Created by PhpStorm.
 * User: kar
 * Date: 5/20/20
 * Time: 8:05 PM
 */

namespace App\Http\Contracts;

/**
 * Interface TypeInterface
 * @package App\Http\Contracts
 */
interface TypeInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function get($id);
}
